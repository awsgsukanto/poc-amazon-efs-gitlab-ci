# POC Amazon EFS Gitlab CI

Proof of concept using Gitlab CI to upload files/artifacts to [Amazon EFS](https://aws.amazon.com/efs/).

It consists in:

| Stage | Job | Description |
|-------|-----|-------------|
| build | build | generate an artifact when running on `main` branch |
| test | unit_tests | continuously run dummy unit test script |
| test | integration_tests | continuously run dummy integration test script |
| test | system_tests | manually run dummy system test script |
| deploy | deploy | continuously run dummy deploy script |
| deploy | upload_artifacts_from_docker | manually run upload file to Amazon EFS. It will create file with size of 10 mb and the filename will be current datetime |


## How to use it

1. Run a pipeline on `main`. This will run the `build` job which generates some artifacts. These artifacts are only generate when running the pipeline on `main`
2. Go to pipeline and hit play/run button at `upload_artifacts_from_docker` job.
